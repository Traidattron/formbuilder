module.exports = {
    semi: true,
    trailingComma: 'all',
    singleQuote: true,
    jsxSingleQuote: true,
    tabWidth: 2,
    jsxBracketSameLine: false,
    printWidth: 120,
};
