const axios = require('axios');

export default {
  uploadImage(data) {
    return axios.post('https://sleepy-tor-00512.herokuapp.com/', data, {}).then((res) => {
      //console.log(res.data)
      return res.data;
    });
  },
};
