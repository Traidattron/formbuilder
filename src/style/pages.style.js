import styled from 'styled-components';
import { Paper, Typography } from '@material-ui/core';
export const ButtonGroupCustom = styled(Paper)`
  position: fixed;
  top: 60vh;
  right: 24%;
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  button {
    margin: 5px 0;
  }
`;
